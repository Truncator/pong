#include <SFML/Graphics.hpp>
#include <vector>
#include "Paddle.h"

class GameObjectManager {
public:
	GameObjectManager();
	~GameObjectManager();

	void add(GameObject* object);
	void renderAll(sf::RenderWindow* window);

private:
	std::vector<GameObject*> m_gameObjects;
};