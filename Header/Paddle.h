#include "GameObject.h"

class Paddle : public GameObject {
public:
	Paddle(int x, int y, int w, int h);

	void render();
};