#include <SFML/Graphics.hpp>
#include "GameObjectManager.h"
#include "Paddle.h"

class Game {
public:
	Game();
	void start();

private:
	enum gameState { mainMenu, playing, paused, exiting };
	gameState m_gameState;

	bool isRunning();
	void gameLoop();

	sf::RenderWindow m_window;
	GameObjectManager m_gameObjectManager;
};