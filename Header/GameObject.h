class GameObject {
public:
	GameObject();
	virtual ~GameObject();

	void move();
	virtual void render();

protected:
	int m_x, m_y;
	int m_w, m_h;
};