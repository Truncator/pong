#include "Game.h"

Game::Game() {
}

void Game::start() {
	m_window.create(sf::VideoMode(640, 480), "Pong");

	m_gameState = mainMenu;

	m_gameObjectManager.add(new Paddle(10, 10, 10, 30));

	while (isRunning()) {
		gameLoop();
	}

	m_window.close();
}

bool Game::isRunning() {
	return (m_gameState != exiting);
}

void Game::gameLoop() {
	sf::Event event;

	while (m_window.pollEvent(event)) {
		m_window.clear(sf::Color::Black);

		switch (m_gameState) {
		case mainMenu:
			{
				if (event.type == sf::Event::KeyPressed) m_gameState = playing;
				break;
			}
		case playing:
			{
				if (event.type == sf::Event::KeyPressed) {
					if (event.key.code == sf::Keyboard::Escape) m_gameState = paused;
				}

				m_window.clear(sf::Color::Black);

				m_gameObjectManager.renderAll(&m_window);

				m_window.display();
			}
		case paused:
			{
				if (event.type == sf::Event::KeyPressed) {
					if (event.key.code == sf::Keyboard::Escape) m_gameState = exiting;
					else m_gameState = playing;
				}
			}
		}
	}
}