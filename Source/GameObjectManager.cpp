#include "GameObjectManager.h"

GameObjectManager::GameObjectManager() {
}

GameObjectManager::~GameObjectManager() {
	for (int i = 0; i < m_gameObjects.size(); i++) {
		delete m_gameObjects[i];
	}
}

void GameObjectManager::add(GameObject* object) {
	m_gameObjects.push_back(object);
}

void GameObjectManager::renderAll(sf::RenderWindow* window) {
	for (int i = 0; i < m_gameObjects.size(); i++) {
		m_gameObjects[i]->render();
	}
}